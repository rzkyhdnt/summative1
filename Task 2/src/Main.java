public class Main {
    public static void main(String[] args){
        Mobil test1 = new Mobil("B 3456 FE", "2021-04-30T13:30:00");
        Motor test2 = new Motor("B 5424 CD", "2021-04-30T14:02:00");
        Motor test3 = new Motor("B 4273 FF", "2021-04-30T17:00:00");
        Mobil test4 = new Mobil("B 6472 DA", "2021-04-30T19:59:00");
        Mobil test5 = new Mobil("D 4321 JH", "2021-04-29T01:00:00");
        //Kondisi 1
        test1.printDetail();

        //Kondisi 2
        test2.printDetail();

        //Kondisi 3
        test3.printDetail();

        //Kondisi 4
        test4.printDetail();

        //Kondisi 5
        test5.printDetail();
    }
}

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.time.LocalDateTime;

public class Mobil extends Parkir{
    public final int tarif = 5000;
    public final int nextTarif = 4500;
    long selisih;
    int totalTarif;;

    public Mobil(String plat, String duration) {
        super(plat, duration);
    }

    public long getSelisih() {
        long selisihJam = Math.abs(timeNow.getHour() - timeDuration.getHour());
        long selisihHari = Math.abs(timeNow.getDayOfMonth() - timeDuration.getDayOfMonth());
        if (selisihHari > 1) {
            selisihJam += (selisihHari * 24);
        }
        selisih = selisihJam;
        return selisih;
    }

    public int getTarif(){
        if (selisih < 1) {
            totalTarif = 1 * tarif;
        } else {
            totalTarif = (int) (tarif + ((selisih - 1) * nextTarif));
        }
        return totalTarif;
    }

    public void printDetail(){
        long hari = 0, jam = 0, menit = 0;
        long selisihHari = Math.abs(timeNow.getDayOfMonth() - timeDuration.getDayOfMonth());
        long selisihJam = Math.abs(timeNow.getHour() - timeDuration.getHour());
        long selisihMenit = Math.abs(timeNow.getMinute() - timeDuration.getMinute());

        if(selisihHari >= 1){
            jam += selisihJam + (selisihHari * 24);
            menit = selisihMenit;
        } else {
            jam = selisihJam;
            menit = selisihMenit;
        }

        DecimalFormat kursIndo = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndo.setDecimalFormatSymbols(formatRp);

        getSelisih();
        getTarif();
        System.out.println("Durasi waktu parkir anda adalah " + jam + "jam : " + menit + "menit");
        System.out.println("Tarif yang harus anda bayar senilai " + kursIndo.format(totalTarif));
    }
}

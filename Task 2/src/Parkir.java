import java.time.LocalDateTime;

public class Parkir {
    String plat;
    LocalDateTime timeDuration;;
    LocalDateTime timeNow = LocalDateTime.parse("2021-04-30T13:00:00");


    public Parkir(String plat, String duration){
        this.plat = plat;
        LocalDateTime durationNow = LocalDateTime.parse(duration);
        this.timeDuration = durationNow;
    }
}

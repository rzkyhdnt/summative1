import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        TrafficLight test = new TrafficLight();
        test.historyTraffic();
        test.printLog();
    }
}

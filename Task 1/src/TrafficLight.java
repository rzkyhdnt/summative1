import java.io.*;
import java.util.ArrayList;

public class TrafficLight {
    ArrayList<String> log = new ArrayList<String>();

    boolean red12 = false;
    boolean orange12 = false;
    boolean green12 = true;

    boolean red3 = true;
    boolean orange3 = false;
    boolean green3 = false;

    boolean red6 = true;
    boolean orange6 = false;
    boolean green6 = false;

    boolean red9 = true;
    boolean orange9 = false;
    boolean green9 = false;


    public void printLog() throws IOException {
        historyTraffic();
        try (BufferedWriter print = new BufferedWriter(new FileWriter("traffic-ligh1.txt"))) {
            for(String l : log){
                print.write(l + System.lineSeparator());
            }
            print.close();
        }
    }

    public void historyTraffic() {
        if (green12) {
            log.add("Lampu merah di traffic light arah jam 3, 6, 9 menyala");
            log.add("Lampu hijau di traffic light arah jam 12 menyala");
            for (int i = 120; i >= 1; i--) {
                log.add("Detik ke " + i);
                if (i == 10) {
                    orange3 = true;
                    red3 = false;
                    log.add("Lampu merah di traffic light arah jam 3 mati");
                    log.add("Lampu orange di traffic light arah jam 3 menyala");
                } else if (i == 1) {
                    red12 = true;
                    green12 = false;
                    orange12 = false;
                    orange3 = false;
                    green3 = true;
                    log.add("Lampu hijau di traffic light arah jam 12 mati");
                    log.add("Lampu merah di traffic light arah jam 12 menyala");
                    log.add("Lampu orange di traffic light arah jam 3 mati");
                    log.add("Lampu hijau di traffic light arah jam 3 menyala");
                }
            }
        }

        if(green3){
            log.add("Lampu merah di traffic light arah jam 12, 6, 9 menyala");
            log.add("Lampu hijau di traffic light arah jam 3 menyala");
            for (int i = 120; i >= 1; i--) {
                log.add("Detik ke " + i);
                System.out.println("Detik ke " + i);
                if (i == 10) {
                    orange6 = true;
                    red6 = false;
                    log.add("Lampu merah di traffic light arah jam 6 mati");
                    log.add("Lampu orange di traffic light arah jam 6 menyala");
                } else if (i == 1) {
                    red3 = true;
                    green3 = false;
                    orange3 = false;
                    orange6 = false;
                    green6 = true;
                    log.add("Lampu hijau di traffic light arah jam 3 mati");
                    log.add("Lampu merah di traffic light arah jam 3 menyala");
                    log.add("Lampu orange di traffic light arah jam 6 mati");
                    log.add("Lampu hijau di traffic light arah jam 6 menyala");
                }
            }
        }

        if(green6){
            log.add("Lampu merah di traffic light arah jam 12, 3, 9 menyala");
            log.add("Lampu hijau di traffic light arah jam 6 menyala");
            for (int i = 120; i >= 1; i--) {
                log.add("Detik ke " + i);
                if (i == 10) {
                    orange9 = true;
                    red9 = false;
                    log.add("Lampu merah di traffic light arah jam 9 mati");
                    log.add("Lampu orange di traffic light arah jam 9 menyala");
                } else if (i == 1) {
                    red6 = true;
                    green6 = false;
                    orange6 = false;
                    orange9 = false;
                    green9 = true;
                    log.add("Lampu hijau di traffic light arah jam 6 mati");
                    log.add("Lampu merah di traffic light arah jam 6 menyala");
                    log.add("Lampu orange di traffic light arah jam 9 mati");
                    log.add("Lampu hijau di traffic light arah jam 9 menyala");
                }
            }
        }

        if(green9){
            log.add("Lampu merah di traffic light arah jam 12, 3, 6 menyala");
            log.add("Lampu hijau di traffic light arah jam 9 menyala");
            for (int i = 120; i >= 1; i--) {
                log.add("Detik ke " + i);
                if (i == 10) {
                    orange12 = true;
                    red12 = false;
                    log.add("Lampu merah di traffic light arah jam 12 mati");
                    log.add("Lampu orange di traffic light arah jam 12 menyala");
                } else if (i == 1) {
                    red9 = true;
                    green9 = false;
                    orange9 = false;
                    orange12 = false;
                    green12 = true;
                    log.add("Lampu hijau di traffic light arah jam 9 mati");
                    log.add("Lampu merah di traffic light arah jam 9 menyala");
                    log.add("Lampu orange di traffic light arah jam 12 mati");
                    log.add("Lampu hijau di traffic light arah jam 12 menyala");
                    System.out.println("Lampu orange di traffic light arah jam 12 mati");
                    System.out.println("Lampu hijau di traffic light arah jam 12 menyala");
                }
            }
        }
    }
}




public class Main {
    public static void main(String[] args){
        Elevator test1 = new Elevator(3);
        test1.getPerson(new int[]{1, -1, -1,});
        test1.getDestination(new int[]{3, 0, 3});
        test1.getRoute();

        System.out.println("\n");

        Elevator test2 = new Elevator(-1);
        test2.getPerson(new int[]{1, 3});
        test2.getDestination(new int[]{-1, -1});
        test2.getRoute();

        System.out.println("\n");

        Elevator test3 = new Elevator(2);
        test3. getPerson(new int[]{3, 1, 0});
        test3.getDestination(new int[]{0, 3, 3});
        test3.getRoute();
    }
}

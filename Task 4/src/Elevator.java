import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Elevator {
    int[] person = new int[5];
    int[] destination = new int[5];
    int floorNow;
    boolean direct = false;

    ArrayList<Integer> getPersonRoute = new ArrayList<>();
    ArrayList<Integer> getDestinationRoute = new ArrayList<>();
    ArrayList<Integer> priority = new ArrayList<>();

    public Elevator(int floorNow) {
        this.floorNow = floorNow;
        System.out.println("Start: " + floorNow);
    }

    public int[] getPerson(int[] person) throws ArrayIndexOutOfBoundsException {
        this.person = person;
        return person;
    }

    public int[] getDestination(int[] destination) throws ArrayIndexOutOfBoundsException {
        this.destination = destination;
        return destination;
    }

    public void getRoute() {
        for (int a : person) {
            getPersonRoute.add(a);
        }
        for (int b : destination) {
            getDestinationRoute.add(b);
        }

        priority();
        dropCustomer();

        priority();
        dropCustomer();
    }

    public void priority(){
        HashMap<Integer, String> floor = new HashMap<Integer, String>();
        floor.put(-1, "Parkir");
        floor.put(0, "Ground Floor");
        floor.put(1, "1");
        floor.put(2, "2");
        floor.put(3, "3");

        int step0 = -1, step1 = 0;
        for(int i = 0; i < getPersonRoute.size(); i++){
            if(step0 < step1){
                step0 = Math.abs(floorNow - getPersonRoute.get(i));
                step1 = step0;
                System.out.println("LT " +  floor.get(getPersonRoute.get(i)) + "-> Get Customer");
                priority.add(getPersonRoute.get(i));
            } else {
                priority.add(getPersonRoute.get(i));
            }
        }
        floorNow = priority.get(0);
    }

    public void dropCustomer(){
        HashMap<Integer, String> floor = new HashMap<Integer, String>();
        floor.put(-1, "Parkir");
        floor.put(0, "Ground Floor");
        floor.put(1, "1");
        floor.put(2, "2");
        floor.put(3, "3");

        Iterator iterator = priority.iterator();

        int i = 0;
        int counter = 0;
        while(iterator.hasNext()){
            if(iterator.next().equals(priority.get(i))){
                direct = true;
                counter++;
            }
            i++;
        }

        if(direct && counter < 5){
            floorNow = getDestinationRoute.get(0);
            System.out.println("LT " +  floor.get(getDestinationRoute.get(0)) + "-> Drop customer");
            getDestinationRoute.remove(0);
            getPersonRoute.remove(0);
        } else if(counter >= 5) {
            System.out.println("LT " +  floor.get(getPersonRoute.get(1)) + "-> Get Customer");
            floorNow = getDestinationRoute.get(0);
            System.out.println("LT " +  floor.get(getDestinationRoute.get(0)) + "-> Drop customer");
            getDestinationRoute.remove(0);
            System.out.println("LT " + floor.get(getDestinationRoute.get(0)) + "-> Drop Customer");
            getPersonRoute.remove(0);
        }
    }
}
